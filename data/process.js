const config = require('../config/config');
const fs = require('fs');
const util = require('util');

let workorders = require('../data/queue_workorders');

async function worker(new_list,callback)
{
    for (key in new_list) {

        //parse the work order payload
        var parse_data = JSON.parse(new_list[key].data);
        var order_item_id = new_list[key].order_item_id;
        var workorder_scp_id = new_list[key].id;

        //get the design id
        var to = parse_data[0].merchant_order_id.lastIndexOf('-');
        to = to == -1 ? parse_data[0].merchant_order_id.length : to + 1;
        var designid = parse_data[0].merchant_order_id.substring(0, to);
        designid = designid.substring(0, designid.length - 1);

        if (new_list[key].id != null) {
            
            var hires_img = await workorders.check_update_hires_img(designid);
            console.log(designid,hires_img)
            for (key in hires_img) {
                var hires_output_url = JSON.parse(hires_img[key]['outputurl']);
                for (key2 in hires_output_url) {
                    if (hires_output_url[key2].order_item_id.indexOf(order_item_id) == -1) {
                        console.log("ERR:: NO HIRES / DTG2GO WORK ORDER GENERATED EITHER MISSING OR MISMATCH FOR ORDER_ITEM_ID#" + order_item_id);
                        continue;
                    }

                    parse_data[0].product_name = parse_data[0].product_name.replace(/\'/g, "");
                    parse_data[0].designs[0].media.url = hires_output_url[key2].url;
                    container = JSON.stringify(parse_data[0]);
                    console.log(container)
                    await workorders.update_hires_img(workorder_scp_id, container)
                    await workorders.send_request(container, workorder_scp_id);

                    
                }
            }
            
        }
    }
}
exports.process_work_orders = async function (data,callback) {
    //fetch new dtg work order list
    var flag = false;
    var new_list = await workorders.fetch_new_list();
    await worker(new_list);
    callback("done");        
};