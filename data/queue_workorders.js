const config = require('../config/config');
const fs = require('fs');
const util = require('util');
var https = require('https');

let settings = require('../data/settings');

exports.fetch_new_list = async function (data = 0) 
{
    
    var listing = []
    var result = await settings.fetch_new_workorders("scp_dtg_work_order", "NEW");
    
    for (key in result)
    {
        var collection = {};        
        for (key2 in result[key]) 
        {
            collection[key2] = result[key][key2];            
        }

        listing.push(collection)     
        if (parseInt(key) + 1 >= 40) 
        {            
            break;
        }
    }    
    return listing;
};

exports.check_update_hires_img = async function (designid) 
{
    var result = await settings.check_vivx_hires_status("vivx_Generatehires", designid, 'Uploaded');
    return result;
};

exports.update_hires_img = async function (id, data) 
{
    var result = await settings.update_hires_image_workorder("scp_dtg_work_order", id, data);
    return result;
};

exports.send_request = async function (payload,id)
{
    re = payload.toString();
    
    // var uri = "/api/v1/workorders";
    // var apikey = "934B8145A36F6A7666376B8A2A827980";
    // var host = "www.dtg2goportal.com";
    var uri = "/api/v1/workorders";
    var apikey = "49F7CC8F3A7ADC1CB2DF564ECA676DC2";
    var host = "sandbox.dtg2goportal.com";
    var options = {
        host: host,
        port: 443,  
        path: uri,  
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'apikey': apikey,
            "Content-Length": Buffer.byteLength(payload)
        },
        
        json: true

    };
    
    var req = https.request(options, function (res) {
        console.log('Status: ' + res.statusCode);
        console.log('Headers: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');
        res.on('data', function (body) {
            console.log('Body: ' + body);
            var result =  JSON.parse(body);
            
            if (result.success != false)
            {
                console.log(result.work_order_id, id)
                settings.insert_workorderid_dtg2go("scp_dtg_work_order", result.work_order_id, id);
                settings.update_workorderstatus_dtg2go("scp_dtg_work_order", 'PROCESSING', id);
            }
            else
            {
                settings.insert_workorderid_dtg2go("scp_dtg_work_order", 'ERR', id);
            }
        });        
    });
    req.on('error', function (e) {
        console.log('problem with request: ' + e.message);
    });
    // write data to request body
    req.write(re);
    req.end();
    
};