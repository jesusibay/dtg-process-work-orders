var pool = require('../config/config');

exports.fetch_new_workorders = async function(table,status){
  try {    
    return await pool.query('SELECT * FROM ' + table + ' WHERE work_order_id ="" AND order_status="'+status+'"');
  } catch(err) {
    //throw new Error(err)
    return err;
  }
}

exports.check_vivx_hires_status = async function (table, designid, status) {
  try {
    return await pool.query('SELECT * FROM ' + table + ' WHERE designid ="' + designid +'" AND s3_status="' + status + '"');
  } catch (err) {
    //throw new Error(err)
    return err;
  }
}

exports.update_hires_image_workorder = async function (table, id, data) {
  try {
    return await pool.query('UPDATE ' + table + ' SET data ="' + data + '" WHERE id="' + id + '"');
  } catch (err) {
    //throw new Error(err)
    return err;
  }
}
exports.insert_workorderid_dtg2go = async function (table, data, id ) {
  try {
    return await pool.query('UPDATE ' + table + ' SET work_order_id ="' + data + '" WHERE id="' + id + '"');
  } catch (err) {
    //throw new Error(err)
    return err;
  }
}
exports.update_workorderstatus_dtg2go = async function (table, data, id) {
  try {
    return await pool.query('UPDATE ' + table + ' SET order_status ="' + data + '" WHERE id="' + id + '"');
  } catch (err) {
    //throw new Error(err)
    return err;
  }
}